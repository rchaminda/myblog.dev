<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', function()
{
	return View::make('hello');
});

/*
|-----------------------------------------------
|routes for public users
|-----------------------------------------------
|
*/
Route::controller('users', 'UserController');



/*
|-----------------------------------------------
|routes for authenticated users
|-----------------------------------------------
|
*/
Route::group(array('before' => 'auth'), function() {
    Route::controller('backend', 'BackendController');
});