@extends('master')

@section('custom_embedded_styles')
<style type="text/css">
.container {
	margin-top:30px;
}
</style>
@stop

@section('content')
<div class="container">
      <div class="col-md-4 col-md-offset-4">
    <div class="panel panel-default">
  <div class="panel-heading"><h3 class="panel-title"><strong>Sign in </strong></h3></div>
  <div class="panel-body">
   {{ Form::open(array('url' => 'users/authenticate', 'role'=>'form')) }}
	  <div class="form-group">
	    <label for="exampleInputEmail1">Email 
	    @if(isset($errMessages))
	    <span style="color: red">
	    	(
		    	@if($errMessages->has('email'))
		    		{{$errMessages->first('email')}}
		    	@elseif($errMessages->has('password'))
		    		{{$errMessages->first('password')}}
		    	@elseif($errMessages->has('result'))
		    		{{$errMessages->first('result')}}
		    	@endif
	    	)
	    </span>
	    @endif
	    </label>
	    {{Form::text('email', Input::old('email'), 
	    						array('class'=>'form-control', 'style'=>'border-radius:0px',
	    								'id'=>'exampleInputEmail1', 'placeholder'=>'Enter email'))}}
	  </div>
	  <div class="form-group">
	    <label for="exampleInputPassword1">Password <a href="/sessions/forgot_password">(forgot password)</a></label>
	    <input name="password" type="password" class="form-control" style="border-radius:0px" id="exampleInputPassword1" placeholder="Password">
	  </div>
	  <button type="submit" class="btn btn-sm btn-default">Sign in</button>
	{{ Form::close() }}
  </div>
</div>
</div>
    </div>
@stop