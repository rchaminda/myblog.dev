<!doctype html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>MYblog</title>
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"/>
	<!-- ################### Master style sheets starts ################### -->
	{{ HTML::style(asset('assets/css/bootstrap.min.css')) }}

	<!-- ################### Master style sheets ends ################### -->

	<!-- ################### View specific style sheets starts ################### -->
	@yield('custom_stylesheets')
	<!-- ################### View specific style sheets ends ################### -->

	<!-- ################### Master script files starts ################### -->
	{{ HTML::script(asset('assets/js/jquery-2.1.1.min.js')) }}
	{{ HTML::script(asset('assets/js/bootstrap.min.js')) }}

	<!-- ################### Master script files ends ################### -->

	<!-- ################### View specific script files starts ################### -->
	@yield('custom_script_files')
	<!-- ################### View specific script files ends ################### -->
	<script type="text/javascript">
		var base_url = "{{URL::secure('/')}}";
	</script>

	<!-- ################### View specific embedded scripting starts  ################### -->
	@yield('custom_embedded_scripts')
	<!-- ################### View specific embedded scripting ends  ################### -->
	
	<!-- ################### View specific embedded styling starts  ################### -->
	@yield('custom_embedded_styles')
	<!-- ################### View specific embedded styling ends  ################### -->
</head>
<body>
		@yield('content')
</body>
</html>
