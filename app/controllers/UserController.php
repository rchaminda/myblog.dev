<?php

class UserController extends \BaseController {

	/**
	 * Display a login form.
	 * GET /users/login
	 *
	 * @return Response
	 */
	public function getLogin()
	{
		return View::make('users.login');
	}

	/**
	 * Authenticate user login data.
	 * POST /users/authenticate
	 *
	 * @return Response
	 */
	public function postAuthenticate()
	{

		Input::flash();

		//setting up validation rules
		$validator = Validator::make(
		    array(	'email'	=>	Input::get('email'),
		    		'password'	=>	Input::get('password')),

		    array(	'email' => 'required|email',
		    		'password'	=>	'required')
		);

		if(!$validator->fails()){
			$userdata = array(	'email'	=>	Input::get('email'),
								'password'	=>	Input::get('password'));
			if(Auth::attempt($userdata)){
				return Redirect::to('backend');
			} else {
				//prepping invalid user message
				$messageBag = new Illuminate\Support\MessageBag;
				$messageBag->add('result', 'Invalid email or password');
				return View::make('users.login')->with('errMessages', $messageBag);
			}

		} else {
			return View::make('users.login')->with('errMessages', $validator->messages());
		}
	}


	/**
	 * Display a listing of the resource.
	 * GET /users
	 *
	 * @return Response
	 */
	public function getLogout()
	{
		Auth::logout();
		return Redirect::to('users/login');
	}

	/**
	 * Display a listing of the resource.
	 * GET /users
	 *
	 * @return Response
	 */
	public function getIndex()
	{
		return View::make('users.index');
	}

	/**
	 * Show the form for creating a new resource.
	 * GET /users/create
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 * POST /users
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 * GET /users/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 * GET /users/{id}/edit
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 * PUT /users/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 * DELETE /users/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}