<?php

// Composer: "fzaninotto/faker": "v1.3.0"
use Faker\Factory as Faker;

class UsersTableSeeder extends Seeder {

	public function run()
	{

		$user = User::create(array(
		  'first_name' => 'Ranil',
		  'last_name' => 'Madawalage',
		  'email' => 'ranil.m@eyepax.com',
		  'password' => Hash::make('123456')
		));

		$faker = Faker::create();

		foreach(range(1, 10) as $index)
		{
			User::create([
			    'first_name' => $faker->firstName,
			    'last_name' => $faker->lastName,
			    'email' => $faker->email,
			    'password' => Hash::make('123456'),
			]);
		}
	}

}